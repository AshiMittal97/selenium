package com.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class SeleniumActions {

	public static void main(String[] args) throws InterruptedException, IOException {
		 WebDriver driverChrome;
		FileInputStream fis = new FileInputStream(new File("E:\\New folder\\AmazonAutomation\\config.properities"));
		Properties props = new Properties();
		props.load(fis);

		String flipkartUrl = props.getProperty("FlipkartUrl");
		String driverUrl = props.getProperty("DriverUrl");
		String path = System.getProperty("user.dir");
		String genericPath = path + driverUrl;
		System.setProperty("webdriver.chrome.driver", genericPath);
		
		driverChrome =  new ChromeDriver();
		
		
		driverChrome.get("http://www.edureka.co/");
		driverChrome.manage().window().maximize();
		driverChrome.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Thread.sleep(3000);
		Actions builder = new Actions(driverChrome);
		builder.moveToElement(driverChrome.findElement(By.id("header_topcat"))).build().perform();
		Thread.sleep(3000);
		WebElement link = driverChrome.findElement(By.cssSelector("#software-testing-certification-courses"));
		builder.moveToElement(link).build().perform();
		Thread.sleep(2000);
		driverChrome.findElement(By.xpath("//ul[@class='dropdown-menu']//li//a[text()='Software Testing']")).click();
		Thread.sleep(4000);
		WebElement act = driverChrome.findElement(By.id("search-inp"));
		builder.moveToElement(act).build().perform();
		Thread.sleep(3000);
		WebElement search = driverChrome.findElement(By.xpath("//span[@class='typeahead__button']"));
		builder.moveToElement(search).build().perform();
		Thread.sleep(3000);

	}

}
