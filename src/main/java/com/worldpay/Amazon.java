package com.worldpay;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Amazon {

	static WebDriver driverChrome;
	static FileInputStream fis;
	static Properties props;
	static String path;

	@BeforeTest
	public static void browser() throws IOException, InterruptedException, FindFailed {

		fis = new FileInputStream(new File("E:\\New folder\\AmazonAutomation\\config.properities"));
		props = new Properties();
		props.load(fis);

		String driverUrl = props.getProperty("DriverUrl");
		 path = System.getProperty("user.dir");
		String genericPath = path + driverUrl;
		System.setProperty("webdriver.chrome.driver", genericPath);
		driverChrome = new ChromeDriver();
		

	}

	@Test(priority = 1)
	public void cart() throws Exception {
		fis = new FileInputStream(new File("E:\\New folder\\AmazonAutomation\\config.properities"));
		props = new Properties();

		props.load(fis);

		String flipkartUrl = props.getProperty("FlipkartUrl");
		//driverChrome.manage().deleteAllCookies();
				String eTitle = "Online Shopping Site for Mobiles, Electronics, Furniture, Grocery, Lifestyle, Books & More. Best Offers!";
				String aTitle = "" ;
				
				driverChrome.get(flipkartUrl);
				//screenshot
				TakesScreenshot scrShot =((TakesScreenshot)driverChrome);

		        //Call getScreenshotAs method to create image file

		                File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);

		            //Move image file to new destination
    
		                
		                File DestFile=new File(path+"//screenshot.png");

		                //Copy file at destination

		                FileUtils.copyFile(SrcFile, DestFile);
			
				//Maximizes the browser window
				driverChrome.manage().window().maximize() ;
				//get the actual value of the title
				aTitle = driverChrome.getTitle();
				//compare the actual title with the expected title
				if (aTitle.equals(eTitle))
				{
				System.out.println( "Test Passed") ;
				}
				else {
				System.out.println( "Test Failed" );
				}
	
		driverChrome.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); // implicit
																				// wait
		driverChrome.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

		driverChrome.manage().window().maximize();
		FileInputStream excel = new FileInputStream(new File("E:\\New folder\\AmazonAutomation\\Excel\\Login.xlsx"));
		XSSFWorkbook woorkbook = new XSSFWorkbook(excel);
		XSSFSheet sheet = woorkbook.getSheet("Sheet1");
		XSSFRow row = sheet.getRow(0);

		String username = row.getCell(0).getStringCellValue();
		String password = row.getCell(1).getStringCellValue();

		driverChrome.findElement(By.xpath("//input[@type=\"text\" and @class=\"_2zrpKA _1dBPDZ\"]")).sendKeys(username);
		driverChrome.findElement(By.xpath("//input[@type=\"password\" and @class=\"_2zrpKA _3v41xv _1dBPDZ\"]"))
				.sendKeys(password);

		driverChrome.findElement(By.xpath("//button[@type=\"submit\"and @class=\"_2AkmmA _1LctnI _7UHT_c\"]")).click();
		Thread.sleep(10000);
		driverChrome.findElement(By.xpath("//input[@type=\"text\" and @name=\"q\"]")).sendKeys("java");
		Thread.sleep(10000);
		driverChrome.findElement(By.xpath("//button[@type=\"submit\" and @class=\"vh79eN\"]")).click();
		Thread.sleep(5000);
		driverChrome.findElement(By.xpath("//a[@class=\"_2cLu-l\"]")).click();
		ArrayList<String> tabs2 = new ArrayList<String>(driverChrome.getWindowHandles());
		driverChrome.switchTo().window(tabs2.get(1));
		driverChrome.findElement(By.id("pincodeInputId")).sendKeys("453441");
		driverChrome.findElement(By.xpath("//span[@class=\"_2aK_gu\"]")).click();
		Thread.sleep(1000);
		driverChrome.findElement(By.xpath("//button[@type=\"button\" and @class=\"_2AkmmA _2Npkh4 _2kuvG8 _7UHT_c\"]"))
				.click();
		Thread.sleep(1000);
		/*//driverChrome.findElement(By.xpath("//input[@type=\"text\" and @class=\"_16qL6K _366U7Q\"]")).sendKeys("Ashi");
		driverChrome.findElement(By.name("phone")).sendKeys("6261352722");
		driverChrome.findElement(By.name("pincode")).sendKeys("453441");
		driverChrome.findElement(By.name("addressLine2")).sendKeys("Gupta Compund");
		driverChrome.findElement(By.name("addressLine1")).sendKeys("Sindhi Colony,Gupta Compund");
		WebElement city = driverChrome.findElement(By.xpath("//select[@name=\"city\"]"));
		Select cityDropDown = new Select(city);
		cityDropDown.selectByVisibleText("Mhow Cantt");

		WebElement element = driverChrome.findElement(By.xpath("//select[@name=\"state\"]"));
		Select countryDropDown = new Select(element);
		countryDropDown.selectByVisibleText("Madhya Pradesh");
		// driverChrome.findElement(By.xpath("//div[@class=\"_6ATDKp\"]")).click();
		driverChrome.findElement(By.xpath("//button[@type=\"button\" and @class=\"_2AkmmA EqjTfe _7UHT_c\"]")).click();*/
		driverChrome.findElement(By.xpath("//button[@class=\"_2AkmmA _2Q4i61 _7UHT_c\"]")).click();
	}

	/*@Test
	public void sikuliMethod() throws FindFailed, InterruptedException {

		Screen screen = new Screen();
		Pattern loginbutton = new Pattern("E:\\New folder\\AmazonAutomation\\ScreenSnapshot");
		screen.click(loginbutton);
		Thread.sleep(10000);

	}
	
*/
	
	
	/*@AfterTest
	public void closeBrowser() {

		driverChrome.close();
		driverChrome.quit();
	}
*/
}
